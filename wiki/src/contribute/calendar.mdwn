[[!meta title="Calendar"]]

* 2014-08-12: Freeze Tails 1.1.1.

* 2014-08-13: build Tails 1.1.1~rc1.

* 2014-08-14: test Tails 1.1.1~rc1.

* 2014-08-15: release Tails 1.1.1~rc1.

* 2014-09-02: Release Tails 1.1.1. intrigeri does the first ~half of
  the RM duty, anonym takes over around 2014-08-20.

* 2014-09-03, 7pm UTC (9pm CEST): Tails contributors meeting
  (`#tails-meeting` on `irc.indymedia.org` / `h7gf2ha3hefoj5ls.onion`)

* 2014-10-14: Release Tails 1.2. anonym is RM.

* 2014-11-25: Release 1.2.1. anonym is RM.

* 2015-01-06: Release 1.2.2. anonym is RM. If Mozilla decides to send
  their employees on vacation, delaying the Firefox ESR release for a
  week or two, anonym can still be RM.

* 2015-02-17: Release 1.3. Still undecided who will be RM, but anonym
  probably can do it.
